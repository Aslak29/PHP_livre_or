<?php

$bddserver = "localhost";
$bddname ="livreor";
$bddlogin ="root";
$bddpass ="";

$paramOk=false;

if(isset($_POST['pseudo'])){
    $pseudo= htmlspecialchars($_POST['pseudo']);
    $paramOk = true;
}if(isset($_POST['commentaire'])){
    $commentaire= htmlspecialchars($_POST['commentaire']);
    $paramOk = true;
}

if($paramOk == true){
    // INSERT dans la base
    try{
        $objBdd= new PDO("mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8",$bddlogin,$bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        $pdoStmt =$objBdd->prepare("INSERT INTO message (pseudo, commentaire) VALUES (:pseudo, :commentaire)");
        $pdoStmt -> bindParam(':pseudo',$pseudo, PDO:: PARAM_STR);
        $pdoStmt -> bindParam(':commentaire',$commentaire, PDO:: PARAM_STR);
        $pdoStmt -> execute();
        
        $lastId=$objBdd->lastInsertId ();

    }catch(Exception $prmE){
        die('Erreur:' .$prmE->getMessage());
    }

    $serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'index.php';
header("Location: http://$serveur$chemin/$page");

 }else{
     die('Les paramètres reçus ne sont pas valides');
 }

?>